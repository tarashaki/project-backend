create table "t_table_user"
(
    "id"         NUMBER(10) generated as identity
        primary key,
    "birthday"   DATE,
    "email"      VARCHAR2(255 char),
    "first_name" VARCHAR2(255 char),
    "last_name"  VARCHAR2(255 char),
    "password"   VARCHAR2(255 char)
)
/

