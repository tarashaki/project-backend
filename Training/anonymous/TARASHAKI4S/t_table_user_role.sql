create table "t_table_user_role"
(
    "user_id" NUMBER(10) not null
        constraint "FKkp1critevwjnb4welud3j1de3"
            references "t_table_user",
    "role_id" NUMBER(10) not null
        constraint "FKe9xnrkw4j1f6pev0y0xt7dplg"
            references "t_table_role",
    primary key ("user_id", "role_id")
)
/

