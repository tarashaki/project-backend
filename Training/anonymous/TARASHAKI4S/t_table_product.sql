create table "t_table_product"
(
    "id"          NUMBER(10) generated as identity
        primary key,
    "active"      NUMBER(1)          not null,
    "image"       VARCHAR2(255 char) not null,
    "name"        VARCHAR2(255 char) not null,
    "price"       NUMBER(10)         not null,
    "category_id" NUMBER(10)         not null
        constraint "FK986a6u2v8nxyk9e50moufg8qg"
            references "t_table_category",
    "quantity"    NUMBER(10)         not null
)
/

