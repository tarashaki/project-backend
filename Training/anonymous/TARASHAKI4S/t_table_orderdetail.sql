create table "t_table_orderdetail"
(
    "id"         NUMBER(10) generated as identity
        primary key,
    "image"      VARCHAR2(255 char),
    "price"      NUMBER(10) not null,
    "quantity"   NUMBER(10) not null,
    "order_id"   NUMBER(10)
        constraint "FKqd2fignpldd2o5ngxydheffxv"
            references "t_table_order",
    "product_id" NUMBER(10)
        constraint "FKjpov03c03i2iaalbekgog64gy"
            references "t_table_product"
)
/

