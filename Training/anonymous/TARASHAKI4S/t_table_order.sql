create table "t_table_order"
(
    "id"           NUMBER(10) generated as identity
        primary key,
    "create_date"  DATE               not null,
    "address"      VARCHAR2(255 char) not null,
    "phone_number" VARCHAR2(255 char) not null,
    "status"       NUMBER(1)          not null,
    "user_id"      NUMBER(10)         not null
        constraint "FK660721wqes3f7lmwwktkar9a4"
            references "t_table_user"
)
/

