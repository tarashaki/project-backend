package com.example.training.service.impl;

import com.example.training.entity.User;
import com.example.training.repositoty.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

  @InjectMocks
  UserServiceImpl userService;

  @Mock
  UserRepository userRepository;

  @BeforeEach
  public void init() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void createUser()
  {
    User user = new User("user@email.com","123123","1","1",new Date());

    userService.create(user);

    verify(userRepository, times(1)).save(user);
    Mockito.lenient().when(userRepository.save(user)).thenReturn(null);
  }

  @Test
  public void createUser_Fail() {
    userService.create(null);
    verify(userRepository, times(1)).save(null);
    Mockito.lenient().when(userRepository.save(null)).thenReturn(null);
  }

  @Test
  public void updateUser() {
    User user = new User();
    user.setRoles(null);
    user.setId(1);
    user.setEmail("test@gmail.com");
    user.setPassword("123123");
    user.setBirthday(new Date(2002,10,10));
    user.setFirstName("Le");
    user.setLastName("Nguyen");
    user.setOrders(null);
    userService.update(user);

    verify(userRepository, times(1)).save(user);
    Mockito.lenient().when(userRepository.save(user)).thenReturn(null);
  }

  @Test
  public void updateUser_Fail() {
    userService.update(null);
    verify(userRepository, times(1)).save(null);
    Mockito.lenient().when(userRepository.save(null)).thenReturn(null);
  }

  @Test
  public void ExistsByEmail(){

    when(userRepository.existsByEmail("test@gmail.com")).
            thenReturn(true);

    Boolean checkEmail = userService.existsByEmail("test@gmail.com");

    assertEquals(true,checkEmail);
  }

  @Test
  public void ExistsByEmail_Fail(){

    when(userRepository.existsByEmail("test123@gmail.com")).
            thenReturn(false);

    Boolean checkEmail = userService.existsByEmail("test123@gmail.com");

    assertEquals(false,checkEmail);
  }

  @Test
  public void findbyEmail(){
    User user = new User();
    user.setRoles(null);
    user.setId(1);
    user.setEmail("test@gmail.com");
    user.setPassword("123123");
    user.setBirthday(new Date());
    user.setFirstName("Le");
    user.setLastName("Nguyen");
    user.setOrders(null);
    when(userRepository.findByEmail("test@gmail.com")).
            thenReturn(Optional.of(user));

    Optional<User> users = userService.findByEmail("test@gmail.com");

    assertEquals(Optional.of(1),Optional.ofNullable(users.get().getId()));
  }

  @Test
  public void findbyEmail_Fail(){
    User user = new User();
    user.setRoles(null);
    user.setId(1);
    user.setEmail("test@gmail.com");
    user.setPassword("123123");
    user.setBirthday(null);
    user.setFirstName("Le");
    user.setLastName("Nguyen");
    user.setOrders(null);
    when(userRepository.findByEmail("test123123@gmail.com")).
            thenReturn(null);

    Optional<User> users = userService.findByEmail("test123123@gmail.com");

    assertEquals(null,users);
  }

}
