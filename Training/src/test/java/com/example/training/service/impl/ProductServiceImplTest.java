package com.example.training.service.impl;

import com.example.training.entity.Category;
import com.example.training.entity.Product;
import com.example.training.repositoty.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProductServiceImplTest {

  @InjectMocks
  private ProductServiceImpl productService;

  @Mock
  private ProductRepository productRepository;

  @BeforeEach
  public void  init(){
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void findByIdTest(){
    Category category = new Category(1,"Nike",null);
    Product product1 = new Product(1,"Nike",1,1,"1.jpg",true,new Date(),category,null);
    Product product2 = new Product(2,"Nike1",2,3,"2.jpg",true,new Date(),category,null);
    List<Product> lstProduct = new ArrayList<Product>();
    lstProduct.add(product1);
    lstProduct.add(product2);

    when(productService.findById(1)).thenReturn(lstProduct.get(0));
    productService.findById(1);

    assertEquals(Optional.of(1), Optional.ofNullable(lstProduct.get(0).getId()));
    verify(productRepository).findProductById(1);

  }

  @Test
  public void findByIdTest_Fail(){
    Category category = new Category(1,"Nike",null);
    Product product1 = new Product(1,"Nike",1,1,"1.jpg",true,new Date(),category,null);
    Product product2 = new Product(2,"Nike1",2,3,"2.jpg",true,new Date(),category,null);
    List<Product> lstProduct = new ArrayList<Product>();
    lstProduct.add(product1);
    lstProduct.add(product2);

    when(productService.findById(3)).thenReturn(null);
    Product result = productService.findById(3);

    assertEquals(null, result);
    verify(productRepository).findProductById(3);

  }

  @Test
  public void findAllProductActiveTest(){
    Category category = new Category(1,"Nike",null);
    Product product=new Product(1,"Nike",1,1,"1.jpg",true,new Date(),category,null);
    Page<Product> page = new PageImpl<Product>(Collections.singletonList(product));



    Pageable paging = PageRequest.of(0, 2);

    when(productService.findAllByActive(paging)).thenReturn((Page<Product>) page);


    productService.findAllByActive(paging);

    assertEquals(1, page.getSize());
    verify(productRepository).findAllByActive(paging);
  }

  @Test
  public void findByCategoryIdTest(){
    Category category = new Category(1,"Nike",null);
    Product product=new Product(1,"Nike",1,1,"1.jpg",true,new Date(),category,null);
    Page<Product> page = new PageImpl<Product>(Collections.singletonList(product));



    Pageable paging = PageRequest.of(0, 2);

    when(productService.findByCategoryId(1,paging)).thenReturn((Page<Product>) page);


    productService.findByCategoryId(1,paging);

    assertEquals(1, page.getSize());
    verify(productRepository).findProductByActiveTrueAndCategoryId(1,paging);
  }

  @Test
  public void findByCategoryIdTest_Fail(){
    Category category = new Category(1,"Nike",null);
    Product product=new Product(1,"Nike",1,1,"1.jpg",true,new Date(),category,null);
    Page<Product> page = new PageImpl<Product>(Collections.singletonList(product));



    Pageable paging = PageRequest.of(0, 2);

    when(productService.findByCategoryId(2,paging)).thenReturn(null);


    Page<Product> result = productService.findByCategoryId(2,paging);

    assertEquals(null ,result);
    verify(productRepository).findProductByActiveTrueAndCategoryId(2,paging);
  }



  @Test
  public void findByNameTest(){
    Category category = new Category(1,"Nike",null);
    Product product=new Product(1,"Nike",1,1,"1.jpg",true,new Date(),category,null);
    Product product1=new Product(1,"Nike1",1,1,"1.jpg",true,new Date(),category,null);
    List<Product> lstProduct = new ArrayList<Product>();
    lstProduct.add(product1);
    lstProduct.add(product);
    Page<Product> page = new PageImpl<Product>(lstProduct);


    Pageable paging = PageRequest.of(0, 2);

    when(productService.findByNamePage("Nike",paging)).thenReturn((Page<Product>) page);


    productService.findByNamePage("Nike",paging);

    assertEquals(2, page.getTotalElements());
    verify(productRepository).findByNamePage("Nike",paging);
  }

  @Test
  public void findByNameTest_NullData(){
    Category category = new Category(1,"Nike",null);
    Product product=new Product(1,"Nike",1,1,"1.jpg",true,new Date(),category,null);
    Product product1=new Product(1,"Nike1",1,1,"1.jpg",true,new Date(),category,null);
    List<Product> lstProduct = new ArrayList<Product>();
    lstProduct.add(product1);
    lstProduct.add(product);
    Page<Product> page = new PageImpl<Product>(lstProduct);


    Pageable paging = PageRequest.of(0, 2);

    when(productService.findByNamePage("",paging)).thenReturn((Page<Product>) page);


    productService.findByNamePage("",paging);

    assertEquals(2, page.getTotalElements());
    verify(productRepository).findByNamePage("",paging);
  }

  @Test
  public void findByPriceTest(){
    Category category = new Category(1,"Nike",null);
    Product product=new Product(1,"Nike",500000,1,"1.jpg",true,
            new Date(),category,null);
    Page<Product> page = new PageImpl<Product>(Collections.singletonList(product));



    Pageable paging = PageRequest.of(0, 2);
    when(productService.findByPrice(1000000,paging)).thenReturn(page);


    productService.findByPrice(1000000,paging);

    assertEquals(1, page.getTotalElements());
    verify(productRepository).findProductByPrice(1000000,paging);
  }

  @Test
  public void findByPriceTest_Fail(){
    Category category = new Category(1,"Nike",null);
    Product product=new Product(1,"Nike",500000,1,"1.jpg",true,
            new Date(),category,null);
    Page<Product> page = new PageImpl<Product>(Collections.singletonList(product));



    Pageable paging = PageRequest.of(0, 2);
    when(productService.findByPrice(0,paging)).thenReturn(null);

    Page<Product> result = productService.findByPrice(0,paging);

    assertEquals(null, result);
    verify(productRepository).findProductByPrice(0,paging);
  }

  @Test
  public void findTop4NewProductTest(){
    Category category = new Category(1,"Nike",null);
    Product product1=new Product(1,"Nike",500000,1,"1.jpg",true,
            new Date(2021,10,10),category,null);
    Product product2=new Product(2,"Nike",500000,1,"1.jpg",true,
            new Date(2022,10,10),category,null);
    Product product3=new Product(3,"Nike",500000,1,"1.jpg",true,
            new Date(2020,10,10),category,null);
    Product product4=new Product(4,"Nike",500000,1,"1.jpg",true,
            new Date(2019,10,10),category,null);
    List<Product> lst = new ArrayList<Product>();
    lst.add(product1);
    lst.add(product2);
    lst.add(product3);
    lst.add(product4);
    Pageable paging = PageRequest.of(0, 4);


    when(productService.findTop4ByNewProduct()).thenReturn(lst);
    productService.findTop4ByNewProduct();

    assertEquals(4, lst.size());
    verify(productRepository).findTop4ByNewProduct(paging);
  }

  @Test
  public void createProductTest() {
    Category category = new Category(1,"Nike",null);
    Product product=new Product(1,"Nike",500000,1,"1.jpg",true,
            new Date(),category,null);

    productService.create(product);

    verify(productRepository, times(1)).save(product);
  }

  @Test
  public void createProductTest_Fail() {
    assertThrows(NullPointerException.class, () -> {
      productService.create(null);
    });

  }


  @Test
  public void updateProductTest() {
    Category category = new Category(2,"Adidas",null);
    Product product=new Product(1,"Adidas 1",500000,1,"1.jpg",true,
            new Date(),category,null);

    productService.update(product);
    verify(productRepository, times(1)).save(product);
  }

  @Test
  public void updateProductTest_Fail() {
    assertThrows(NullPointerException.class, () -> {
      productService.update(null);
    });
  }

  @Test
  public void updateProductByOrderTest() {
    Category category = new Category(2,"Adidas",null);
    Product product=new Product(1,"Adidas 1",500000,13,"1.jpg",true,
            new Date(),category,null);

    productService.updateByOrder(product);
    assertEquals(12, product.getQuantity());
    verify(productRepository, times(1)).save(product);
  }

  @Test
  public void deleteProductTest() {
    Category category = new Category(1,"Nike",null);
    Product product = new Product(1,"Nike",500000,1,"1.jpg",true,
            new Date(),category,null);

    productService.delete(product);
    assertEquals(false, product.isActive());
    verify(productRepository, times(1)).save(product);
  }

  @Test
  public void deleteProductTest_Fail() {
    assertThrows(NullPointerException.class, () -> {
      productService.delete(null);
    });
  }


}
