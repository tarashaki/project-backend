package com.example.training.service.impl;

import com.example.training.entity.Order;
import com.example.training.entity.OrderDetail;
import com.example.training.repositoty.OrderDetailRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OrderDetailServiceImplTest {
  @InjectMocks
  OrderDetailServiceImpl orderDetailService;

  @Mock
  OrderDetailRepository orderDetailRepository;

  @BeforeEach
  public void init() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void getOrderDetailByOrderTest() {
    Order order = new Order();
    order.setId(1);
    List<OrderDetail> lstOrderDetail = new ArrayList<OrderDetail>();
    OrderDetail orderDetail1 = new OrderDetail(1,"1.jpg",123,12,order,null);
    OrderDetail orderDetail2 = new OrderDetail(2,"2.jpg",1230000,12,order,null);
    lstOrderDetail.add(orderDetail1);
    lstOrderDetail.add(orderDetail2);
    orderDetailRepository.findOrderDetailByOrderId(order.getId());
    when(orderDetailRepository.findOrderDetailByOrderId(1)).thenReturn(lstOrderDetail);

    List<OrderDetail> list = orderDetailService.getOrderDetailByOrder(order.getId());
    System.out.println(list);

    assertEquals("1.jpg", list.get(0).getImage());

  }

  @Test
  public void getOrderDetailByOrderTest_Fail() {
    Order order = new Order();
    order.setId(1);
    List<OrderDetail> lstOrderDetail = new ArrayList<OrderDetail>();
    OrderDetail orderDetail1 = new OrderDetail(1,"1.jpg",123,12,order,null);
    OrderDetail orderDetail2 = new OrderDetail(2,"2.jpg",1230000,12,order,null);
    lstOrderDetail.add(orderDetail1);
    lstOrderDetail.add(orderDetail2);
    orderDetailRepository.findOrderDetailByOrderId(order.getId());
    when(orderDetailRepository.findOrderDetailByOrderId(2)).thenReturn(null);

    List<OrderDetail> list = orderDetailService.getOrderDetailByOrder(2);
    System.out.println(list);

    assertEquals(null,list);

  }
}
