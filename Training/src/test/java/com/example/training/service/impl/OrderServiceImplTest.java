package com.example.training.service.impl;

import com.example.training.entity.Order;
import com.example.training.entity.OrderDetail;
import com.example.training.entity.User;
import com.example.training.repositoty.OrderRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceImplTest {

  @InjectMocks
  private OrderServiceImpl orderService;

  @Mock
  private OrderRepository orderRepository;

  @BeforeEach
  public void  init() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void createOrder(){
    ObjectMapper mapper = new ObjectMapper();
    User user = new User();
    user.setId(1);
    Order order = new Order();
    order.setId(12);
    order.setStatus(false);
    order.setCreateDate(new Date(2022 - 11 - 11));
    order.setAddress("123123123");
    order.setUser(user);
    order.setPhoneNumber("076238542");
    List<OrderDetail> lstOrderDetail = new ArrayList<OrderDetail>();
    OrderDetail orderDetail1 = new OrderDetail(1,"1.jpg",123,12,order,null);
    OrderDetail orderDetail2 = new OrderDetail(2,"2.jpg",1230000,12,order,null);
    lstOrderDetail.add(orderDetail1);
    lstOrderDetail.add(orderDetail2);
    order.setOrderDetails(lstOrderDetail);

    ObjectMapper om = new ObjectMapper();

    JsonNode node = mapper.convertValue(order
            , JsonNode.class);
    System.out.println(node);

    orderService.create(node);

    verify(orderRepository, times(1)).save(order);
    lenient().when(orderService.create(node)).thenReturn(order);
  }

  @Test
  public void createOrder_Fail(){
    assertThrows(NullPointerException.class, () -> {
      orderService.create(null);
    });

  }

  @Test
  public void updateStatus() {
      Order order = new Order();
      order.setId(1);
      order.setAddress("HCM");
      order.setPhoneNumber("0706238542");
      order.setCreateDate(new Date());
      order.setStatus(true);
      orderService.updateStatus(order);

      verify(orderRepository, times(1)).save(order);
      Mockito.lenient().when(orderRepository.save(order)).thenReturn(null);
  }

  @Test
  public void updateStatus_Fail() {
    orderService.updateStatus(null);
    verify(orderRepository, times(1)).save(null);
    Mockito.lenient().when(orderRepository.save(null)).thenReturn(null);
  }

  @Test
  public void getOrderByUser() {
    User user = new User();
    user.setId(1);
    List<Order> lstOrder = new ArrayList<Order>();
    Order order1 = new Order(1,"HCM","070623854",true,new Date(),user,null);
    Order order2 = new Order(1,"Đà Nẵng","070623854",true,new Date(),user,null);
    lstOrder.add(order1);
    lstOrder.add(order2);
    when(orderRepository.findAllByUserId(1)).thenReturn(lstOrder);

    List<Order> list = orderService.getOrderByUser(1);

    assertEquals("Đà Nẵng", list.get(1).getAddress());
  }

  @Test
  public void getOrderByUser_Fail() {
    User user = new User();
    user.setId(1);
    List<Order> lstOrder = new ArrayList<Order>();
    Order order1 = new Order(1,"HCM","070623854",true,new Date(),user,null);
    Order order2 = new Order(1,"Đà Nẵng","070623854",true,new Date(),user,null);
    lstOrder.add(order1);
    lstOrder.add(order2);
    when(orderRepository.findAllByUserId(2)).thenReturn(null);

    List<Order> list = orderService.getOrderByUser(2);

    assertEquals(null,list);
  }

}
