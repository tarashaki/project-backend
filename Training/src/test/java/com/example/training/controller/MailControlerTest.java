package com.example.training.controller;

import com.example.training.common.RoleConstants;
import com.example.training.entity.Category;
import com.example.training.entity.Product;
import com.example.training.entity.Role;
import com.example.training.entity.User;
import com.example.training.service.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class MailControlerTest {

  @MockBean
  private EmailService emailService;

  @MockBean
  private UserService userService;

  @Autowired
  private MockMvc mvc;


  ObjectMapper om = new ObjectMapper();

  @Test
  void sendMail() throws Exception {
    Mockito.when(emailService.sendSimpleMail("vnguyen2k2@gmail.com"))
            .thenReturn(123);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                    .get("/home/mail/send-mail?email=vnguyen2k2@gmail.com")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals("123", mvcResult.getResponse().getContentAsString());
  }

  @Test
  void sendMail_Fail() throws Exception {
    Mockito.when(emailService.sendSimpleMail(""))
            .thenReturn(0);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                    .get("/home/mail/send-mail?email=")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals("0", mvcResult.getResponse().getContentAsString());
  }


  @Test
  void checkVerificationNumber() throws Exception {
    Mockito.when(emailService.checkVerificationNumber(123))
            .thenReturn(true);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                    .get("/home/mail/check/123")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals("true", mvcResult.getResponse().getContentAsString());
  }

  @Test
  void checkVerificationNumber_Fail() throws Exception {
    Mockito.when(emailService.checkVerificationNumber(123))
            .thenReturn(true);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                    .get("/home/mail/check/124")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals("false", mvcResult.getResponse().getContentAsString());
  }

  @Test
  void findByEmail() throws Exception {
    User user = new User(1,"vnguyen2k2@gmail.com","123","Nguyen"
    ,"Le",new Date(),null,null);
    User user1 = new User(2,"vnguyen@gmail.com","123","Nguyen"
            ,"Le",new Date(),null,null);
    List<User> lst = new ArrayList<User>();
    lst.add(user);
    lst.add(user1);
    Mockito.when(userService.findByEmail("vnguyen2k2@gmail.com")).thenReturn(Optional.ofNullable(lst.get(0)));
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                    .get("/home/mail/find-by-email?email=vnguyen2k2@gmail.com")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals(200, mvcResult.getResponse().getStatus());
  }

  @Test
  void findByEmail_Fail() throws Exception {
    User user = new User(1,"vnguyen2k2@gmail.com","123","Nguyen"
            ,"Le",new Date(),null,null);
    User user1 = new User(2,"vnguyen@gmail.com","123","Nguyen"
            ,"Le",new Date(),null,null);
    List<User> lst = new ArrayList<User>();
    lst.add(user);
    lst.add(user1);
    Mockito.when(userService.findByEmail("vnguyen2k21@gmail.com")).thenReturn(null);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                    .get("/home/mail/find-by-email?email=vnguyen2k21@gmail.com")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals("", mvcResult.getResponse().getContentAsString());
  }

  @Test
  public void changePassword() throws Exception {
    User user = new User(1,"vnguyen2k2@gmail.com","123","Nguyen"
            ,"Le",new Date(),null,null);
    MvcResult result = mvc.perform(put("/home/mail/change-password")
            .content(om.writeValueAsString(user))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
    String resultContext = result.getResponse().getContentAsString();
    assertEquals(200,result.getResponse().getStatus());
  }

  @Test
  public void changePassword_Fail() throws Exception {
    MvcResult result = mvc.perform(put("/home/mail/change-password")
            .content(om.writeValueAsString(null))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest()).andReturn();
    String resultContext = result.getResponse().getContentAsString();
    assertEquals(400,result.getResponse().getStatus());
  }
}
