package com.example.training.controller;

import com.example.training.entity.*;
import com.example.training.service.CategoryService;
import com.example.training.service.LoadProductService;
import com.example.training.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class ProductControllerTest {

  @MockBean
  private ProductService productService;

  @MockBean
  private CategoryService categoryService;

  @Autowired
  private MockMvc mvc;


  ObjectMapper om = new ObjectMapper();

  @Test
  void getAllProduct() throws Exception {
    Category category = new Category(1,"Nike",null);
    Product product1 = new Product(1,"Nike",1,1,"1.jpg",true,new Date(),category,null);
    Product product2 = new Product(2,"Nike 1",1000,1,"2.jpg",true,new Date(),category,null);
    List<Product> lstproduct = new ArrayList<Product>();
    lstproduct.add(product1);
    lstproduct.add(product2);
    Page<Product> page = new PageImpl<Product>(lstproduct);
    Pageable paging = PageRequest.of(0, 8);
    Mockito.when(productService.findAllByActive(paging)).thenReturn(page);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.
            get("/admin/product/get-all-product-by-admin")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals(200, mvcResult.getResponse().getStatus());
  }

  @Test
  void getAllProductCollection() throws Exception {
    Category category = new Category(1,"Nike",null);
    Product product1 = new Product(1,"Nike",1,1,"1.jpg",true,new Date(),category,null);
    Product product2 = new Product(2,"Nike 1",1000,1,"2.jpg",true,new Date(),category,null);
    List<Product> lstproduct = new ArrayList<Product>();
    lstproduct.add(product1);
    lstproduct.add(product2);
    Page<Product> page = new PageImpl<Product>(lstproduct);
    Pageable paging = PageRequest.of(0, 2);
    Mockito.when(productService.findAllByActive(paging)).thenReturn(page);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.
                    get("/admin/product/collection?page=0&price=500000&sortType=1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals(200, mvcResult.getResponse().getStatus());
  }

  @Test
  void getCategory() throws Exception {
    Category category1 = new Category(1,"Nike",null);
    Category category2 = new Category(2,"Adidas",null);
    List<Category> lstCategory = new ArrayList<Category>();
    lstCategory.add(category1);
    lstCategory.add(category2);
    Mockito.when(categoryService.findByAll()).thenReturn(lstCategory);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.
                    get("/admin/product/getcategory")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals(200, mvcResult.getResponse().getStatus());
  }

  @Test
  void getTop4Product() throws Exception {
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.
                    get("/admin/product/top-4-bestseller")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals(200, mvcResult.getResponse().getStatus());
  }

  @Test
  void getTop4NewProduct() throws Exception {
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.
                    get("/admin/product/top-4-new-product")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals(200, mvcResult.getResponse().getStatus());
  }

  @Test
  void updateByOrder() throws Exception {
    Category category = new Category(1,"Nike",null);
    Product product1 = new Product(1,"Nike",1,1,"1.jpg",true,new Date(),category,null);
    Product product2 = new Product(2,"Nike 1",1000,1,"2.jpg",true,new Date(),category,null);
    List<Product> lstproduct = new ArrayList<Product>();
    lstproduct.add(product1);
    lstproduct.add(product2);
    Page<Product> page = new PageImpl<Product>(lstproduct);
    Pageable paging = PageRequest.of(0, 2);
    Mockito.when(productService.findAllByActive(paging)).thenReturn(page);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.
                    get("/admin/product/update-by-order/1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals(200, mvcResult.getResponse().getStatus());
  }

  @Test
  void getProductByName() throws Exception {
    Category category = new Category(1,"Nike",null);
    Product product1 = new Product(1,"Nike",1,1,"1.jpg",true,new Date(),category,null);
    Product product2 = new Product(2,"Nike 1",1000,1,"2.jpg",true,new Date(),category,null);
    List<Product> lstProduct = new ArrayList<Product>();
    lstProduct.add(product1);
    lstProduct.add(product2);
    Mockito.when(productService.findById(1)).thenReturn(lstProduct.get(0));
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.
                    get("/admin/product/search?name=Nike")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals(200, mvcResult.getResponse().getStatus());
  }

  @Test
  void getProductById() throws Exception {
    Category category = new Category(1,"Nike",null);
    Product product1 = new Product(1,"Nike",1,1,"1.jpg",true,new Date(),category,null);
    Product product2 = new Product(2,"Nike 1",1000,1,"2.jpg",true,new Date(),category,null);
    List<Product> lstProduct = new ArrayList<Product>();
    lstProduct.add(product1);
    lstProduct.add(product2);
    Mockito.when(productService.findById(1)).thenReturn(lstProduct.get(0));
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.
                    get("/admin/product/1")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals(200, mvcResult.getResponse().getStatus());
  }

  @Test
  void getProductById_Fail() throws Exception {
    Category category = new Category(1,"Nike",null);
    Product product1 = new Product(1,"Nike",1,1,"1.jpg",true,new Date(),category,null);
    Product product2 = new Product(2,"Nike 1",1000,1,"2.jpg",true,new Date(),category,null);
    List<Product> lstProduct = new ArrayList<Product>();
    lstProduct.add(product1);
    lstProduct.add(product2);
    Mockito.when(productService.findById(3)).thenReturn(null);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.
                    get("/admin/product/3")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals("", mvcResult.getResponse().getContentAsString());
  }

  @Test
  public void createProductAPI_Fail() throws Exception {
    Category category = new Category(1,"Nike",null);

    MvcResult result = mvc.perform(post("/admin/product/add")
            .content(om.writeValueAsString(null))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)).andReturn();
    String resultContext = result.getResponse().getContentAsString();
    assertEquals(400,result.getResponse().getStatus());
  }

  @Test
  public void createProductAPI() throws Exception {
    Category category = new Category(1,"Nike",null);

    MvcResult result = mvc.perform(post("/admin/product/add")
            .content(om.writeValueAsString(new Product(50,"Nike",1,1,"1.jpg",true,new Date(),category,null)))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)).andReturn();
    String resultContext = result.getResponse().getContentAsString();
    assertEquals(200,result.getResponse().getStatus());
  }

  @Test
  public void updateProductAPI() throws Exception {
    Category category = new Category(1,"Nike",null);

    MvcResult result = mvc.perform(put("/admin/product/update")
            .content(om.writeValueAsString(new Product(50,"Nike",1,1,"1.jpg",true,new Date(),category,null)))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
    String resultContext = result.getResponse().getContentAsString();
    assertEquals(200,result.getResponse().getStatus());
  }

  @Test
  public void updateProductAPI_Fail() throws Exception {
    Category category = new Category(1,"Nike",null);

    MvcResult result = mvc.perform(put("/admin/product/update")
            .content(om.writeValueAsString(null))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)).andReturn();
    String resultContext = result.getResponse().getContentAsString();
    assertEquals(400,result.getResponse().getStatus());
  }

  @Test
  public void deleteProductAPI() throws Exception {
    MvcResult result = mvc.perform(delete("/admin/product/delete/1")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
    String resultContext = result.getResponse().getContentAsString();
    assertEquals(200,result.getResponse().getStatus());
  }

  @Test
  public void deleteProductAPI_Fail() throws Exception {
    MvcResult result = mvc.perform(delete("/admin/product/delete/123123")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
    String resultContext = result.getResponse().getContentAsString();
    assertEquals("",result.getResponse().getContentAsString());
  }
}
