package com.example.training.controller;

import com.example.training.entity.User;
import com.example.training.presentation.LoginRequest;
import com.example.training.presentation.SignupRequest;
import com.example.training.service.EmailService;
import com.example.training.service.LoginService;
import com.example.training.service.RegisterService;
import com.example.training.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class AuthControllerTest {

  @MockBean
  private UserService userService;

  @Autowired
  private MockMvc mvc;

  ObjectMapper om = new ObjectMapper();

  @Test
  public void signup() throws Exception {
    SignupRequest signupRequest = new SignupRequest(
            "vnguyen2k2@gmail.com","123",
            "Nguyen","Le",new Date(), null);
    MvcResult result = mvc.perform(post("/auth/signup")
            .content(om.writeValueAsString(signupRequest))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
    assertEquals(200,result.getResponse().getStatus());
  }

  @Test
  public void signup_Fail() throws Exception {
    MvcResult result = mvc.perform(post("/auth/signup")
            .content(om.writeValueAsString(null))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest()).andReturn();
    assertEquals(400,result.getResponse().getStatus());
  }

  @Test
  public void signin() throws Exception {
    LoginRequest loginRequest = new LoginRequest(
            "vnguyen2k2@gmail.com","123");
    MvcResult result = mvc.perform(post("/auth/signin")
            .content(om.writeValueAsString(loginRequest))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
    assertEquals(200,result.getResponse().getStatus());
  }

  @Test
  public void signin_Fail() throws Exception {
    MvcResult result = mvc.perform(post("/auth/signin")
            .content(om.writeValueAsString(null))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest()).andReturn();
    assertEquals(400,result.getResponse().getStatus());
  }

  @Test
  void checkEmail() throws Exception {
    User user = new User(1,"vnguyen2k2@gmail.com","123","Nguyen"
            ,"Le",new Date(),null,null);
    User user1 = new User(2,"vnguyen@gmail.com","123","Nguyen"
            ,"Le",new Date(),null,null);
    List<User> lst = new ArrayList<User>();
    lst.add(user);
    lst.add(user1);
    Mockito.when(userService.existsByEmail("vnguyen2k2@gmail.com")).
            thenReturn(true);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                    .get("/auth/check-email?email=vnguyen2k2@gmail.com")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
    assertEquals(200, mvcResult.getResponse().getStatus());
  }

  @Test
  void checkEmail_Fail() throws Exception {
    User user = new User(1,"vnguyen2k2@gmail.com","123","Nguyen"
            ,"Le",new Date(),null,null);
    User user1 = new User(2,"vnguyen@gmail.com","123","Nguyen"
            ,"Le",new Date(),null,null);
    List<User> lst = new ArrayList<User>();
    lst.add(user);
    lst.add(user1);
    Mockito.when(userService.existsByEmail("vnguyen123@gmail.com")).
            thenReturn(false);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                    .get("/auth/check-email?email=vnguyen123@gmail.com")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON))
            .andReturn();
   assertEquals("false", mvcResult.getResponse().getContentAsString());
  }
}
