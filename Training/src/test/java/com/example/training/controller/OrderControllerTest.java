package com.example.training.controller;

import com.example.training.entity.Order;
import com.example.training.entity.OrderDetail;
import com.example.training.entity.User;
import com.example.training.service.OrderDetailService;
import com.example.training.service.OrderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.*;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class OrderControllerTest {

  @MockBean
  private OrderService orderService;

  @MockBean
  private OrderDetailService orderDetailService;

  @Autowired
  private MockMvc mvc;

  ObjectMapper om = new ObjectMapper();

  @Test
   void getOrderList() throws Exception {
    Pageable paging = PageRequest.of(0, 5);
    Order order1 =  new Order(1,"123","123",true,new Date(),null,null);
    Order order2 =  new Order(2,"123","123",true,new Date(),null,null);
    List<Order> lst = new ArrayList<Order>();
    lst.add(order1);
    lst.add(order2);
    Page<Order> page = new PageImpl<>(lst);
    Mockito.when(orderService.findAll(paging)).thenReturn(page );
    mvc.perform(
                    get("/home/order/find-all")
                            .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk());

  }

  @Test
  public void getOrderDetailByOrderAPI() throws Exception {
    String uri = "/home/order/get-order-detail-by-order/1";
    List<OrderDetail> lstOrderDetail=orderDetailService.getOrderDetailByOrder(1);

    when(orderDetailService.getOrderDetailByOrder(1)).thenReturn(lstOrderDetail);
    mvc.perform(
                    get(uri)
                            .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk());
  }

  @Test
  public void getOrderDetailByOrderAPI_Fail() throws Exception {
    String uri = "/home/order/get-order-detail-by-order/2";
    List<OrderDetail> lstOrderDetail=orderDetailService.getOrderDetailByOrder(1);

    when(orderDetailService.getOrderDetailByOrder(2)).thenReturn(null);
    MvcResult result = mvc.perform(
                    get(uri)
                            .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andReturn();
    assertEquals("",result.getResponse().getContentAsString());
  }

  @Test
  public void getOrderByUserAPI() throws Exception {
    String uri = "/home/order/get-order-by-user/1";
    List<Order> lstOrder=orderService.getOrderByUser(1);

    when(orderService.getOrderByUser(1)).thenReturn(lstOrder);
    mvc.perform(
                    get(uri)
                            .contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk());
  }

  @Test
  public void getOrderByUserAPI_Fail() throws Exception {
    String uri = "/home/order/get-order-by-user/1";
    List<Order> lstOrder=orderService.getOrderByUser(1);

    when(orderService.getOrderByUser(2)).thenReturn(null);
    MvcResult result = mvc.perform(
                    get(uri)
                            .contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();
    assertEquals("[]",result.getResponse().getContentAsString());
  }

  @Test
  public void updateStatusOrderAPI() throws Exception {
    User user = new User();
    user.setId(1);
    OrderDetail orderDetail = new OrderDetail();
    orderDetail.setId(1);
    orderDetail.setImage("1.jpg");
    orderDetail.setPrice(123123);
    orderDetail.setQuantity(1);
    List<OrderDetail> lstOrderDedtail= new ArrayList<OrderDetail>();
       Order order = new Order(1,"HCM","123123",true,new Date(),user,lstOrderDedtail);
    MvcResult result = mvc.perform(put("/home/order/update-order")
            .content(om.writeValueAsString(order))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
    String resultContext = result.getResponse().getContentAsString();
    assertEquals(200,result.getResponse().getStatus());
  }

  @Test
  public void updateOrderAPI_Fail() throws Exception {
    MvcResult result = mvc.perform(put("/home/order/update-order")
            .content(om.writeValueAsString(null))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)).andReturn();
    assertEquals("",result.getResponse().getContentAsString());
  }



  @Test
  public void createOrderAPI() throws Exception {
    User user = new User();
    user.setId(1);
    OrderDetail orderDetail = new OrderDetail();
    orderDetail.setId(1);
    orderDetail.setImage("1.jpg");
    orderDetail.setPrice(123123);
    orderDetail.setQuantity(1);
    List<OrderDetail> lstOrderDedtail= new ArrayList<OrderDetail>();
    MvcResult result = mvc.perform(post("/home/order/add")
            .content(om.writeValueAsString(new Order(1, "HCM", "0706238542", false, new Date(), user, lstOrderDedtail)))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
    String resultContext = result.getResponse().getContentAsString();
    assertEquals(200,result.getResponse().getStatus());
  }

  @Test
  public void createOrderAPI_Fail() throws Exception {
    MvcResult result = mvc.perform(post("/home/order/add")
            .content(om.writeValueAsString(null))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)).andReturn();
    assertEquals("",result.getResponse().getContentAsString());
  }

}
