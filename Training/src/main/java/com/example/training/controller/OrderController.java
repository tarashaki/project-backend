package com.example.training.controller;


import com.example.training.common.MessageConstants;
import com.example.training.entity.Order;
import com.example.training.entity.OrderDetail;
import com.example.training.presentation.MessageResponse;
import com.example.training.service.OrderDetailService;
import com.example.training.service.OrderService;
import com.example.training.service.impl.OrderServiceImpl;
import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/**
 * The OrderControler is a class to perform the functions of Order
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-11-04
 * @update 2022-12-12
 *
 */
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("home/order")
public class OrderController {

  private static final Logger logger = LoggerFactory.getLogger(OrderController.class);
  @Autowired
  private OrderService orderService;


  @Autowired
  private OrderDetailService orderDetailService;

  /**
   * add Order funtion,
   * @param orderData: add order and order Detail
   */
  @PostMapping("/add")
  @PreAuthorize("hasRole('ROLE_USER')")
  public Order create(@RequestBody JsonNode orderData) {
    try {
      return orderService.create(orderData);
    } catch (Exception e) {
      ResponseEntity.badRequest().body(new MessageResponse("Error: Can't create order"));
      logger.error(MessageConstants.CANNOT_CREATE_ORDER, e);
      return null;
    }
  }


  /**
   * get Order by User funtion,
   * @param userId: find Order by Userid
   */
  @GetMapping("/get-order-by-user/{userid}")
  @PreAuthorize("hasRole('ROLE_USER')")
  public List<Order> getOrderByUser(@PathVariable("userid") Integer userId) {
    try {
      return orderService.getOrderByUser(userId);
    } catch (Exception e){
      logger.error("Can't get Order by User", e);
      return null;
    }
  }

  /**
   * get Order by id funtion,
   * @param orderid: find Order by orderid
   */
  @GetMapping("/get-order-detail-by-order/{orderid}")
  public List<OrderDetail> getOrderDetailByOrder(@PathVariable("orderid") Integer orderid) {
    try {
      return orderDetailService.getOrderDetailByOrder(orderid);
    } catch (Exception e){
      logger.error("Can't find OrderDetail by OrderID", e);
      return null;
    }

  }

  /**
   * getAll Orderfuntion,
   * @param page: pagination order
   */
  @GetMapping("/find-all")
  public Page<Order> findAllOrder(@RequestParam("page") Optional<Integer> page) {
    try {
      Pageable pageable = PageRequest.of(page.orElse(0), 5);
      return  orderService.findAll(pageable);
    } catch (Exception e){
        logger.error("Can't get All Order", e);
        return null;
    }

  }

  /**
   * update status order funtion,
   * @param order : update status order
   */
  @PutMapping("/update-order")
  public Order update(@RequestBody Order order) {
    try {
      return orderService.updateStatus(order);
    } catch (Exception e) {
      ResponseEntity.badRequest().body(new MessageResponse("Error: Can't update status order"));
      logger.error(MessageConstants.CANNOT_UPDATE_STATUS_ORDER, e);
      return null;
    }
  }

  /**
   * total price funtion,
   */
  @GetMapping("/total")
  public int total() {
    return orderService.totalOrder();
  }

  /**
   * total price by Id funtion,
   */
  @GetMapping("/total-by-product-id")
  public int totalByProductId(@RequestParam("productid") int productid) {
    return orderDetailService.totalByOrder(productid);
  }

  /**
   * sum price funtion,
   */
  @GetMapping("/sum-price")
  public int sumPrice() {
    return orderDetailService.sumprice();
  }

  /**
   * sum price by Month funtion,
   */
  @GetMapping("/sum-price-by-month")
  public int sumPriceByMonth(@RequestParam("month") int month, @RequestParam("year") int year) {
    return orderDetailService.sumPriceByMonth(month, year);
  }
}
