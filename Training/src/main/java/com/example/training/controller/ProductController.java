package com.example.training.controller;

import com.example.training.common.MessageConstants;
import com.example.training.entity.Category;
import com.example.training.entity.Product;
import com.example.training.presentation.ProductDTO;
import com.example.training.presentation.MessageResponse;
import com.example.training.service.CategoryService;
import com.example.training.service.LoadProductService;
import com.example.training.service.ProductService;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * The ProductController is a class to perform the functions of Product
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-25
 * @update 2022-12-15
 */
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/admin/product")
public class ProductController {

  private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

  @Autowired
  private ProductService productService;

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private LoadProductService loadProductService;

  /**
   * getAllProduct  funtion,
   * @param page: pagination product
   * @param price:filter by price
   * @param categoryId:filter by categoryId
   * @param sortType:Sort type product
   */
  @GetMapping("/collection")
  public Page<Product> findAll(@RequestParam("page") Optional<Integer> page,
                               @RequestParam("price") Optional<Integer> price,
                               @RequestParam("categoryId") Optional<Integer> categoryId,
                               @RequestParam("sortType") Optional<Integer> sortType) {
    try {
      return loadProductService.findAll(page, price, categoryId, sortType);
    } catch (Exception e){
      logger.error("Can't find All Product", e);
      return null;
    }

  }


  /**
   * getAllProductByAdmin  funtion,
   * @param page: pagination product
   */
  @GetMapping("/get-all-product-by-admin")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public Page<Product> findAllByAdmin(@RequestParam("page") Optional<Integer> page) {
    try {
      Pageable pageable = PageRequest.of(page.orElse(0), 8, Sort.Direction.DESC, "id");
      return productService.findAllByActive(pageable);
    } catch (Exception e){
      logger.error("Can't find All Product By Admin", e);
      return null;
    }

  }


  /**
   * GetAllCategory funtion,
   *
   */
  @GetMapping("/getcategory")
  public List<Category> findCategory() {
    try {
      return categoryService.findByAll();
    } catch (Exception e){
      logger.error("Can't Category", e);
      return null;
    }
  }

  /**
   * Get top 4 product funtion,
   *
   */
  @GetMapping("/top-4-bestseller")
  public List<ProductDTO> findTop4Product() {
    try {
      return productService.findTop4Product();
    } catch (Exception e){
      logger.error("Can't get top 4 Product bestseller", e);
      return null;
    }
  }

  /**
   * Get top 4 new product funtion,
   *
   */
  @GetMapping("/top-4-new-product")
  public List<Product> findTop4NewProduct() {
    try {
      return productService.findTop4ByNewProduct();
    } catch (Exception e){
      logger.error("Can't get top 4 New Product", e);
      return null;
    }
  }

  /**
   * get Product by id funtion,
   * @param id: get Product by id
   */
  @GetMapping("{id}")
  public Product findIdProductById(@PathVariable("id") Integer id)  {
    try {
      return productService.findById(id);
    } catch (Exception e){
      logger.error("Can't get Product By Id", e);
      return null;
    }
  }

  /**
   * add product funtion,
   * @param product: product will add
   */
  @PostMapping("/add")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public Product create(@RequestBody Product product) {
    try {
      return productService.create(product);
    } catch (Exception e) {
      ResponseEntity.badRequest().body(new MessageResponse("Error: Can't add product"));
      logger.error(MessageConstants.CANNOT_ADD_PRODUCT, e);
      return null;
    }
  }

  /**
   * update funtion,
   * @param product: product will update by id
   */
  @PutMapping("/update")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public Product update(@RequestBody Product product) {
    try {
      return productService.update(product);
    } catch (Exception e) {
      ResponseEntity.badRequest().body(new MessageResponse("Error: Can't update Product"));
      logger.error(MessageConstants.CANNOT_UPDATE_PRODUCT, e);
      return null;
    }
  }

  /**
   * delete product funtion,
   * @param id: product will set active = false by id
   */
  @DeleteMapping("/delete/{id}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public void delete(@PathVariable("id") Integer id) {
    try {
      Product product = productService.findById(id);
      productService.delete(product);
    } catch (Exception e) {
      ResponseEntity.badRequest().body(new MessageResponse("Error delete product"));
      logger.error(MessageConstants.CANNOT_DELETE_PRODUCT, e);
    }

  }

  /**
   * update funtion,
   * @param id: product will update by id when user buy shoes
   */
  @GetMapping("/update-by-order/{id}")
  public Product updateByOrder(@PathVariable("id") Integer id) {
    try {
      Product product = productService.findById(id);
      return productService.updateByOrder(product);
    } catch (Exception e) {
      ResponseEntity.badRequest().body(new MessageResponse("Error: Can't update Product"));
      logger.error(MessageConstants.CANNOT_UPDATE_PRODUCT_BY_ORDER, e);
      return null;
    }
  }


  /**
   * search product by Name  funtion,
   * @param request: name will search
   * @param page:pagination
   */
  @GetMapping("/search/{info}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public ResponseEntity<Page<Product>> findProductByName(
          @PathVariable("info")Optional<String> request,
          @RequestParam("page") Optional<Integer> page) {
    try {
      String kwords = request.orElse("");
      Pageable pageable = PageRequest.of(page.orElse(0), 8);
      Page<Product> pages = productService.findByNamePage(kwords, pageable);
      return ResponseEntity.ok(pages);

    } catch (Exception e) {
      logger.error(MessageConstants.CANNOT_SEARCH_PRODUCT, e);
      return null;
    }
  }

  /**
   * search product by name  funtion,
   * @param request: name product will search
   */
  @GetMapping("/search")
  public List<Product> findProductByName(@RequestParam("name")Optional<String> request) {
    try {
      String kwords = request.orElse("");
      return productService.findByName(kwords);
    } catch (Exception e) {
      logger.error(MessageConstants.CANNOT_SEARCH_PRODUCT_IN_MENU, e);
      return Collections.emptyList();
    }

  }
}
