package com.example.training.controller;

import com.example.training.common.MessageConstants;
import com.example.training.entity.User;
import com.example.training.presentation.MessageResponse;
import com.example.training.service.EmailService;
import com.example.training.service.UserService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

/**
 * The MailControler is a class to perform the functions of email
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-11-18
 * @since 2022-11-21
 */
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/home/mail")
public class MailController {

  private static final Logger logger = LoggerFactory.getLogger(MailController.class);

  @Autowired
  private EmailService emailService;

  @Autowired
  private PasswordEncoder encoder;

  @Autowired
  private UserService userService;

  /**
   * Send Mail funtion,
   * @param email: That email will be sent authentic code
   */
  @GetMapping("/send-mail")
  public int sendMail(@RequestParam("email") String email) {
    try {
      return emailService.sendSimpleMail(email);
    } catch (Exception e) {
      logger.error(MessageConstants.CANNOT_SEND_MAIL, e);
      return 0;
    }

  }


  /**
   * check Code Authentication funtion,
   * @param numberRandom: Code authentication after the user enters
   */
  @GetMapping("/check/{numberrandom}")
  public boolean checkVerificationNumber(@PathVariable("numberrandom") int numberRandom) {
    return emailService.checkVerificationNumber(numberRandom);
  }

  /**
   * find Email funtion,
   * @param email: Search for that email in the database
   */
  @GetMapping("/find-by-email")
  public  Optional<User> findByEmail(@RequestParam("email") String email) {
    try {
      return userService.findByEmail(email);
    } catch (Exception e) {
      ResponseEntity.badRequest().body(new MessageResponse("Error: Can't find Email!"));
      logger.error(MessageConstants.CANNOT_FIND_EMAIL, e);
      return Optional.empty();
    }
  }

  /**
   * Change password funtion,
   * @param user: update password user
   */
  @PutMapping("/change-password")
  public User changePassword(@RequestBody User user) {
    try {
      String encodedPassword = encoder.encode(user.getPassword());
      user.setPassword(encodedPassword);
      return userService.update(user);
    } catch (Exception e) {
      ResponseEntity.badRequest().body(new MessageResponse("Error: Can't change passwords!"));
      logger.error(MessageConstants.CANNOT_CHANGE_PASSWORD, e);
      return null;
    }
  }
}
