package com.example.training.controller;

import com.example.training.common.MessageConstants;
import com.example.training.presentation.LoginRequest;
import com.example.training.presentation.MessageResponse;
import com.example.training.presentation.SignupRequest;
import com.example.training.repositoty.JwtResponse;
import com.example.training.service.LoginService;
import com.example.training.service.RegisterService;
import com.example.training.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * The AuthController is a class to perform the functions of logging, registering
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-24
 *
 */
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/auth")
public class AuthController {

  private static final Logger logger = LoggerFactory.getLogger(AuthController.class);
  @Autowired
  private RegisterService registerService;

  @Autowired
  private LoginService loginService;

  @Autowired
  private UserService userService;

  /**
   * Login funtion,
   * @param loginRequest: value get when login
   */
  @PostMapping("/signin")
  public ResponseEntity<JwtResponse> authenticateUser(@Validated @RequestBody
                                                      LoginRequest loginRequest) {
    try {
      return  loginService.authenticateUser(loginRequest);
    } catch (Exception e){
      logger.error("Can't login",e);
      return null;
    }
  }

  /**
   * Login funtion,
   * @param signupRequest: value get when register
   */
  @PostMapping("/signup")
  public ResponseEntity<MessageResponse> registerUser(@Validated @RequestBody
                                                      SignupRequest signupRequest) {
    try {
      return  registerService.registerUser(signupRequest);
    } catch (Exception e){
      logger.error("Can't register", e);
      return null;
    }
  }

  /**
   * checkmail funtion,
   * @param email: check email exist database
   */
  @GetMapping("/check-email")
  public boolean checkEmail(@RequestParam("email") String email) {
    if (Boolean.TRUE.equals(userService.existsByEmail(email))) {
      return true;
    } else {
      // will be invoked for both email == false
      logger.error(MessageConstants.CANNOT_FIND_EMAIL);
      return false;
    }
  }

}
