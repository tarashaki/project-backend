package com.example.training.common;

/**
 * The ProductSortType is the class save static variables will be used
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-12-13
 *
 */
public class ProductSortType {
  private ProductSortType() {
    throw new IllegalStateException("ProductSortType class");
  }
  public static final int NAME_ASCENDING = 1;
  public static final int NAME_DESCENDING = 2;
  public static final int PRICE_ASCENDING = 3;
  public static final int PRICE_DESCENDING = 4;
}
