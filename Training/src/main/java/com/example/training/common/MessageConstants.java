package com.example.training.common;

/**
 * The MessageConstants is the class save static variables will be used
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-12-15
 *
 */
public class MessageConstants {

  private MessageConstants() {
    throw new IllegalStateException("MessageConstants class");
  }

  //Product
  public static final String CANNOT_ADD_PRODUCT = "Can't add product";
  public static final String CANNOT_UPDATE_PRODUCT = "Can't update product";
  public static final String CANNOT_UPDATE_PRODUCT_BY_ORDER = "Can't update product by order";
  public static final String CANNOT_DELETE_PRODUCT = "Can't delete product";
  public static final String CANNOT_SEARCH_PRODUCT = "Can't search product";
  public static final String CANNOT_SEARCH_PRODUCT_IN_MENU = "Can't add product in menu";

  //Order
  public static final String CANNOT_UPDATE_STATUS_ORDER = "Can't update status order";
  public static final String CANNOT_CREATE_ORDER = "Can't add  order";

  //Email
  public static final String CANNOT_SEND_MAIL = "Can't send mail";
  public static final String CANNOT_CHANGE_PASSWORD = "Can't change password";
  public static final String CANNOT_FIND_EMAIL = "Can't find email";
  //Register
  public static final String CANOT_FIND_ROLE = "Role is not found";
}
