package com.example.training.common;

import com.example.training.service.impl.UserDetailsImpl;
import java.util.Date;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * The JwtUtils Is the class contains methods that will be done related to JWT
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-24
 *
 */
@Component
public class JwtUtils {
  private static final String SECRET_KEY = "123456789";
  private static final long EXPIRE_TIME = 86400000000L;
  private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class.getName());

  /**
   * gennerate Token funtion,
   * @param authentication: generateTokenLogin
   */
  public String generateTokenLogin(Authentication authentication) {
    UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();

    return Jwts.builder()
            .setSubject((userPrincipal.getUsername()))
            .setIssuedAt(new Date())
            .setExpiration(new Date((new Date()).getTime() + EXPIRE_TIME * 1000))
            .signWith(SignatureAlgorithm.HS512, SECRET_KEY)
            .compact();
  }

  /**
   * validate token funtion,
   * @param authToken: token
   */
  public boolean validateJwtToken(String authToken) {
    try {
      Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(authToken);
      return true;
    } catch (MalformedJwtException e) {
      logger.error("Invalid JWT token -> Message: {}", e);
    } catch (ExpiredJwtException e) {
      logger.error("Expired JWT token -> Message: {}", e);
    } catch (UnsupportedJwtException e) {
      logger.error("Unsupported JWT token -> Message: {}", e);
    } catch (IllegalArgumentException e) {
      logger.error("JWT claims string is empty -> Message: {}", e);
    }

    return false;
  }
  /**
   * get Usernane From Token,
   * @param token: token
   */
  public String getUserNameFromJwtToken(String token) {

    String userName = Jwts.parser()
            .setSigningKey(SECRET_KEY)
            .parseClaimsJws(token)
            .getBody().getSubject();
    return userName;
  }

}
