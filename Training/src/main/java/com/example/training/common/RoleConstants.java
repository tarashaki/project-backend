package com.example.training.common;

/**
 * The RoleConstants is the enum save static variables will be used
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-24
 *
 */
public enum RoleConstants {
    ROLE_USER,
    ROLE_ADMIN
}
