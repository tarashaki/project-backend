package com.example.training.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Order is the class Let mapping to table t_table_order in database
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-11-03
 *
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "T_TABLE_ORDER")
public class Order implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  private String address;
  private String phoneNumber;
  private boolean status;
  @Temporal(TemporalType.DATE)
  private Date createDate = new Date();

  @ManyToOne
  @JoinColumn(name = "user_id")
  private User user;
  @JsonIgnore
  @OneToMany(mappedBy = "order")
  private List<OrderDetail> orderDetails;
}
