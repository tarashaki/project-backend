package com.example.training.entity;

import java.io.Serializable;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The OrderDetail is the class Let mapping to table t_table_orderDetail in database
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-11-03
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "T_TABLE_ORDERDETAIL")
public class OrderDetail implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String image;
  private int price;
  private int quantity;

  @ManyToOne
  @JoinColumn(name = "order_id")
  private Order order;

  @ManyToOne
  @JoinColumn(name = "product_id")
  private Product product;
}
