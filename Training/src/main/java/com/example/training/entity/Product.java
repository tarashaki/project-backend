package com.example.training.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Product is the class Let mapping to table t_table_product in database
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "T_TABLE_PRODUCT")
public class Product implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  private String name;
  private int price;
  private int quantity;
  private String image;
  private boolean active;

  private Date createDate;

  @ManyToOne
  @JoinColumn(name = "category_id")
  Category category;

  @JsonIgnore
  @OneToMany(mappedBy = "product")
  private List<OrderDetail> orderDetails;


}
