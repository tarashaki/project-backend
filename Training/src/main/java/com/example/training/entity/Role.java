package com.example.training.entity;

import com.example.training.common.RoleConstants;
import java.io.Serializable;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Role is the class Let mapping to table t_table_role in database
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "T_TABLE_ROLE")
public class Role implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Enumerated(EnumType.STRING)
  @Column(length = 20)
  private RoleConstants name;
}
