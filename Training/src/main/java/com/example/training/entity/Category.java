package com.example.training.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.persistence.*;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The Category is the class Let mapping to table t_table_category in database
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "T_TABLE_CATEGORY")
public class Category implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String name;

  @JsonIgnore
  @OneToMany(mappedBy = "category")
  private List<Product> products;

}
