package com.example.training.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The User is the class Let mapping to table t_table_user in database
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "T_TABLE_USER")
public class User implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String email;

  private String password;

  private String firstName;

  private String lastName;

  private Date birthday;

  @JsonIgnore
  @OneToMany(mappedBy = "user")
  private List<Order> orders;

  @OneToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "T_TABLE_USER_ROLE",
          joinColumns = @JoinColumn(name = "userId"),
          inverseJoinColumns = @JoinColumn(name = "roleId"))


  private Set<Role> roles = new HashSet<>();

  public User(String email, String password, String firstName, String lastName, Date birthday) {
    this.email = email;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthday = birthday;
  }

  public Set<Role> getRoles() {
    return roles;
  }

  public void setRoles(Set<Role> roles) {
    this.roles = roles;
  }
}
