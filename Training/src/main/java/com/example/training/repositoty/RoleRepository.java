package com.example.training.repositoty;

import com.example.training.common.RoleConstants;
import com.example.training.entity.Role;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The Role is the class write query to table t_table_role in database
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
  Optional<Role> findByName(RoleConstants name);
}
