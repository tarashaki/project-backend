package com.example.training.repositoty;

import com.example.training.entity.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * The UserRepository is the class write query to table t_table_usser in database
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

  Optional<User> findByEmail(String email);

  Boolean existsByEmail(String email);

}
