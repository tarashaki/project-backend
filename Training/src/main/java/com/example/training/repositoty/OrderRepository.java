package com.example.training.repositoty;

import com.example.training.entity.Order;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * The OrderRepository is the class write query to table t_table_order in database
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-11-03
 *
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
  @Query("SELECT o FROM Order o WHERE o.user.id = ?1 ORDER BY o.createDate DESC")
  List<Order> findAllByUserId(Integer userId);

  Page<Order> findByStatusFalse(Pageable pageable);

  @Query("SELECT COUNT(o) FROM Order o")
  int totalOrder();



}
