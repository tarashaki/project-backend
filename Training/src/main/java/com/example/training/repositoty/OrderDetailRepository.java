package com.example.training.repositoty;

import com.example.training.entity.OrderDetail;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * The OrderDetailRepository is the class write query to table t_table_orderDetail in database
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-11-03
 *
 */
@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail, Integer> {
  List<OrderDetail> findOrderDetailByOrderId(Integer orderId);

  @Query("SELECT sum(o.price*o.quantity) FROM OrderDetail o WHERE o.product.id=?1")
  int totalOrderByProduct(int productid);

  @Query("SELECT SUM(o.price*o.quantity) FROM OrderDetail o")
  int sumPrice();

  @Query("SELECT COUNT(o.id) FROM OrderDetail o")
  int quantitySold();

  @Query(value = "select sum(\"price\"*\"quantity\") as dtthang\n"
          + "from \"t_table_orderdetail\"\n"
          + "join \"t_table_order\" tto on tto.\"id\" = \"t_table_orderdetail\".\"order_id\"\n"
          + "where EXTRACT(month FROM \"create_date\") =?1 AND "
          + "EXTRACT(year FROM \"create_date\") =?2",
          nativeQuery = true)
  int sumPriceByMonth(int month, int year);

  @Query("SELECT product.name,product.id,count(tto.product.id) as cpid,"
          + "tto.product.id  from  Product "
          + "product inner join OrderDetail tto on product.id = tto.product.id"
          + " where product.id=tto.product.id and  product.active = true \n"
          + "group by tto.product.id,product.name,product.id "
          + "order by cpid DESC")
  List<Object[]> findTop1QuantitySold(Pageable pageable);

  @Query("SELECT product.name,product.id,count(tto.product.id) as cpid,\n"
          + "       tto.product.id  from  Product "
          + "product inner join OrderDetail tto on product.id = tto.product.id"
          + " where product.id=tto.product.id and  product.active = true \n"
          + "group by tto.product.id,product.name,product.id "
          + "order by cpid")
  List<Object[]> findTop1QuantitySoldLeast(Pageable pageable);

  @Query("SELECT COUNT(o.product.id) as a,o.product.id from OrderDetail o \n"
          + "GROUP BY o.product.id \n"
          + "ORDER BY a")
  List<Object[]> statistic(Pageable pageable);
}
