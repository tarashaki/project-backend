package com.example.training.repositoty;

import com.example.training.entity.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The CategoryRepository is the class write query to table t_table_category in database
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
public interface CategoryRepository extends JpaRepository<Category, Integer> {
  Page<Category> findByName(String keywords, Pageable pageable);
}
