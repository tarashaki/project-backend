package com.example.training.repositoty;

import com.example.training.entity.Product;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * The ProductRepository is the class write query to table t_table_product in database
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
  @Query("SELECT sp FROM Product sp where sp.name like %?1% AND sp.active = true")
  Page<Product> findByNamePage(String keywords, Pageable pageable);

  @Query("SELECT sp FROM Product sp WHERE sp.id = ?1 AND sp.active = true")
  Product findProductById(Integer id);

  @Query("SELECT sp FROM Product sp WHERE sp.active = true")
  Page<Product> findAllByActive(Pageable page);

  @Query("SELECT sp FROM Product sp where sp.name like %?1% and sp.active=true")
  List<Product> findByName(String keywords);

  @Query("SELECT product.name,product.id,product.price,product.image,"
          + "product.active,count(tto.product.id) as cpid,\n"
          + " tto.product.id  from  Product "
          + "product inner join OrderDetail tto on product.id = tto.product.id"
          + " where product.id=tto.product.id and  product.active = true \n"
          + "group by tto.product.id,product.name,product.id,"
          + "product.price,product.active,product.image "
          + "order by cpid DESC")
  List<Object[]> findTop4ByActiveTrue(Pageable pageable);

  @Query("SELECT sp FROM Product sp WHERE sp.active = true ORDER BY sp.createDate DESC")
  List<Product> findTop4ByNewProduct(Pageable pageable);

  @Query("SELECT sp FROM Product sp WHERE sp.active = true AND sp.category.id =?1")
  Page<Product> findProductByActiveTrueAndCategoryId(Integer categoryId, Pageable pageable);

  @Query("SELECT sp FROM Product sp where sp.price <=?1 AND sp.active=true")
  Page<Product> findProductByPrice(Integer price, Pageable pageable);

}
