package com.example.training.repositoty;

import java.util.Date;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * The JwtResponse is class is required for creating a response
 * containing the JWT to be returned to the user.
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-22
 *
 */
@Getter
@Setter
public class JwtResponse {

  private String token;

  private String type = "Bearer";

  private Integer id;

  private String email;


  private String firstName;


  private String lastName;


  private Date birthday;

  private List<String> roles;


  public JwtResponse(String accessToken, Integer id, String email, String firstName,
                     String lastName, Date birthday, List<String> roles) {
    this.token = accessToken;
    this.id = id;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthday = birthday;
    this.roles = roles;
  }

}
