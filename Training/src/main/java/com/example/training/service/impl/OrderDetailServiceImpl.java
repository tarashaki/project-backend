package com.example.training.service.impl;

import com.example.training.entity.OrderDetail;
import com.example.training.repositoty.OrderDetailRepository;
import com.example.training.service.OrderDetailService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The OrderDetailServiceImpl is the class that  implements interface OrderDetailService
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-11-03
 *
 */
@Service
public class OrderDetailServiceImpl implements OrderDetailService {

  @Autowired
  private OrderDetailRepository orderDetailRepository;

  /**
   * create Order funtion,
   * @param orderDetail: create orderDetail
   */
  @Override
  public OrderDetail create(OrderDetail orderDetail) {
    return orderDetailRepository.save(orderDetail);
  }

  /**
   * delete Order funtion,
   * @param id: delete orderDetail By Id
   */
  @Override
  public void deleteById(Integer id) {
    orderDetailRepository.deleteById(id);
  }

  /**
   * update Order funtion,
   * @param orderDetail: update orderDetail
   */
  @Override
  public OrderDetail update(OrderDetail orderDetail) {
    return orderDetailRepository.save(orderDetail);
  }

  /**
   * get Order Detail By Order funtion,
   * @param orderid: get orderedetail by orderID
   */
  @Override
  public List<OrderDetail> getOrderDetailByOrder(Integer orderid) {
    return orderDetailRepository.findOrderDetailByOrderId(orderid);
  }

  /**
   * Get product revenue funtion,
   * @param productid: get orderedetail by orderID
   */
  @Override
  public int totalByOrder(Integer productid) {
    return orderDetailRepository.totalOrderByProduct(productid);
  }

  /**
   * Revenue statistics funtion,
   */
  @Override
  public int sumprice() {
    return orderDetailRepository.sumPrice();
  }

  /**
   * Revenue statistics by month funtion,
   */
  @Override
  public int sumPriceByMonth(int month, int year) {
    return orderDetailRepository.sumPriceByMonth(month, year);
  }

}
