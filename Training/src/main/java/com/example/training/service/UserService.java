package com.example.training.service;

import com.example.training.entity.User;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * The UserService is the interface write database query methods will be done
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
public interface UserService {
  public Optional<User> findByEmail(String email);

  public Boolean existsByEmail(String email);

  public User create(User user);

  public User update(User user);

  public void delete(Integer id);

  public Page<User> findAll(Pageable page);


}
