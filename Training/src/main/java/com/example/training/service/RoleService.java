package com.example.training.service;

import com.example.training.common.RoleConstants;
import com.example.training.entity.Role;
import java.util.Optional;

/**
 * The RoleService is the interface write database query methods will be done
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
public interface RoleService {
  public Optional<Role> findByName(RoleConstants name);
}
