package com.example.training.service.impl;

import com.example.training.common.RoleConstants;
import com.example.training.entity.Role;
import com.example.training.repositoty.RoleRepository;
import com.example.training.service.RoleService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The RoleServiceImpl is the class that  implements interface RoleService
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
@Service
public class RoleServiceImpl implements RoleService {

  @Autowired
  private RoleRepository roleRepository;

  /**
   * get role by name funtion
   * @param name:name will be find
   */
  @Override
  public Optional<Role> findByName(RoleConstants name) {
    return roleRepository.findByName(name);
  }
}
