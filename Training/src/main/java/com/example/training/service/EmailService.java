package com.example.training.service;

import com.example.training.entity.User;

/**
 * The EmailService is the interface write query methods will be done
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-11-17
 *
 */
public interface EmailService {

  // Method
  // To send a simple email
  int sendSimpleMail(String email);

  boolean checkVerificationNumber(int numberRandom);

  User changePass(User user);
}
