package com.example.training.service;

import com.example.training.entity.Category;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * The UserService is the interface write the database query methods will be done
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
public interface CategoryService {
  public Page<Category> findByName(String keywords, Pageable pageable);

  public List<Category> findByAll();

  public Optional<Category> findById(Integer id);
}
