package com.example.training.service;

import com.example.training.entity.Order;
import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * The OrderService is the interface write database query methods will be done
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-11-03
 *
 */
public interface OrderService {

  public Page<Order> findAll(Pageable pageable);

  public Order create(JsonNode order);

  public Order updateStatus(Order order);

  List<Order> getOrderByUser(Integer userid);

  int totalOrder();

}
