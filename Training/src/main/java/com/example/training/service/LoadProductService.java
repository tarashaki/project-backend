package com.example.training.service;

import com.example.training.common.ProductSortType;
import com.example.training.entity.Product;
import com.example.training.presentation.MessageResponse;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * LoginService is a class that helps to perform loadProducts function
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-12-15
 *
 */
@Service
public class LoadProductService {

  private static final Logger logger = LoggerFactory.getLogger(
          LoadProductService.class);

  @Autowired
  private ProductService productService;

  /**
   * getAllProduct  funtion,
   * @param page: pagination product
   * @param price: filter by price
   * @param categoryId: filter by Category
   * @param sortType : sort type product
   */
  public Page<Product> findAll(@RequestParam("page") Optional<Integer> page,
                               @RequestParam("price") Optional<Integer> price,
                               @RequestParam("categoryId") Optional<Integer> categoryId,
                               @RequestParam("sortType") Optional<Integer> sortType) {
    try {
      Pageable pageable = PageRequest.of(page.orElse(0), 8);

      if (sortType.isPresent()) {
        if (sortType.get() == ProductSortType.NAME_ASCENDING) {
          pageable = PageRequest.of(page.orElse(0), 8, Sort.by(Sort.Direction.ASC, "name"));
        } else if (sortType.get() == ProductSortType.NAME_DESCENDING) {
          pageable = PageRequest.of(page.orElse(0), 8, Sort.by(Sort.Direction.DESC, "name"));
        } else if (sortType.get() == ProductSortType.PRICE_ASCENDING) {
          pageable = PageRequest.of(page.orElse(0), 8, Sort.by(Sort.Direction.ASC, "price"));
        } else if (sortType.get() == ProductSortType.PRICE_DESCENDING) {
          pageable = PageRequest.of(page.orElse(0), 8, Sort.by(Sort.Direction.DESC, "price"));
        }
      }
      if (categoryId.get() != 0) {
        return productService.findByCategoryId(categoryId.get(), pageable);
      } else if (price.get() != 0) {
        return productService.findByPrice(price.get(), pageable);
      } else {
        return productService.findAllByActive(pageable);
      }

    } catch (Exception e) {
      ResponseEntity.badRequest().body(new MessageResponse("Error: Can't add product"));
      logger.error("Erorr: Can't load product");
      return null;
    }

  }
}
