package com.example.training.service;

import com.example.training.entity.Product;
import com.example.training.presentation.ProductDTO;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * The ProductService is the interface write database query methods will be done
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
public interface ProductService {
  public Page<Product> findAllByActive(Pageable page);
  public Page<Product> findByCategoryId(Integer id, Pageable page);

  public Page<Product> findByNamePage(String keywords, Pageable pageable);

  public Page<Product> findByPrice(Integer price, Pageable pageable);

  public Product create(Product product);

  public Product update(Product product);

  public Product updateByOrder(Product product);

  public void delete(Product product);

  public Product  findById(Integer id);

  public List<ProductDTO> findTop4Product();


  public List<Product> findTop4ByNewProduct();

  public List<Product> findByName(String keyword);

}
