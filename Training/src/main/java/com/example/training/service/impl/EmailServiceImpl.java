package com.example.training.service.impl;

import com.example.training.entity.User;
import com.example.training.repositoty.UserRepository;
import com.example.training.service.EmailService;
import com.example.training.service.UserService;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**
 * The EmailServiceImpl is the class that  implements interface EmailService
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-11-17
 *
 */
@Service
public class EmailServiceImpl implements EmailService {

  @Autowired
  private JavaMailSender javaMailSender;

  @Autowired
  private UserService userService;

  @Autowired
  private UserRepository userRepository;



  @Value("${spring.mail.username}")
  private String sender;
  int min = 100;
  int max = 999;
  int randomnumber;
  Random random = new Random();

  /**
   * Send Mail funtion,
   * @param email: email address will be sent
   */
  // To send a simple email
  public int sendSimpleMail(String email) {
    // Try block to check for exceptions
    randomnumber = random.nextInt(999);
    try {
      if (Boolean.TRUE.equals(userService.existsByEmail(email))) {

        // Creating a simple mail message
        SimpleMailMessage mailMessage
                = new SimpleMailMessage();

        // Setting up necessary details
        mailMessage.setFrom(sender);
        mailMessage.setTo(email);
        mailMessage.setText("Mã xác nhận của bạn là : " + randomnumber);
        mailMessage.setSubject("Gửi mã xác nhận");

        // Sending the mail
        javaMailSender.send(mailMessage);
        return randomnumber;
      }
    } catch (Exception e) {
      return 0;
    }
    return 0;
  }

  /**
   * Check Verification Number funtion,
   * @param numberRandom: Verification Number
   */
  public boolean checkVerificationNumber(int numberRandom) {
    //checkVerificationNumber
    if (numberRandom == randomnumber) {
      randomnumber = random.nextInt(999);
      return true;
    }
    return  false;
  }

  /**
   * Change password funtion,
   * @param user: User will change password
   */
  @Override
  public User changePass(User user) {
    //change password
    return userRepository.save(user);
  }
}
