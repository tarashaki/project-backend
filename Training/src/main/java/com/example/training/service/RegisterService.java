package com.example.training.service;

import com.example.training.common.MessageConstants;
import com.example.training.common.RoleConstants;
import com.example.training.entity.Role;
import com.example.training.entity.User;
import com.example.training.presentation.MessageResponse;
import com.example.training.presentation.SignupRequest;
import com.example.training.service.impl.RoleServiceImpl;
import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;


/**
 * RegisterService is a class that helps to perform register function
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-11-17
 *
 */
@Service
public class RegisterService {

  private static final Logger logger = LoggerFactory.getLogger(RegisterService.class);

  @Autowired
  private UserService userService;

  @Autowired
  private RoleService roleService;

  @Autowired
  private PasswordEncoder encoder;

  /**
   * Login funtion,
   * @param signupRequest: value get when register
   */
  public ResponseEntity<MessageResponse> registerUser(@Validated @RequestBody
                                                      SignupRequest signupRequest) {
    try {
      if (Boolean.TRUE.equals(userService.existsByEmail(signupRequest.getEmail()))) {
        logger.error("Đã có người dùng email này");
        return ResponseEntity.badRequest()
                .body(new MessageResponse("Error: User is already taken!"));
      }

      User user = new User(signupRequest.getEmail(),
              encoder.encode(signupRequest.getPassword()), signupRequest.getFirstName(),
              signupRequest.getLastName(), signupRequest.getBirthday());

      Set<String> strRoles = signupRequest.getRole();
      Set<Role> roles = new HashSet<>();
      if (strRoles == null) {
        Role userRole = roleService.findByName(RoleConstants.ROLE_USER)
                .orElseThrow(() -> new RuntimeException(MessageConstants.CANOT_FIND_ROLE));
        roles.add(userRole);
      } else {
        strRoles.forEach(role -> {
          switch (role) {
            case "admin":
              Role adminRole = roleService.findByName(RoleConstants.ROLE_ADMIN)
                      .orElseThrow(() -> new RuntimeException(MessageConstants.CANOT_FIND_ROLE));
              roles.add(adminRole);
              break;
            case "user":
              Role modRole = roleService.findByName(RoleConstants.ROLE_USER)
                      .orElseThrow(() -> new RuntimeException(MessageConstants.CANOT_FIND_ROLE));
              roles.add(modRole);
              break;
            default:
              Role userRole = roleService.findByName(RoleConstants.ROLE_USER)
                      .orElseThrow(() -> new RuntimeException(MessageConstants.CANOT_FIND_ROLE));
              roles.add(userRole);
          }
        });
      }

      user.setRoles(roles);
      userService.create(user);

      return  ResponseEntity.ok(new MessageResponse("User reigistered succesfully!"));
    } catch (Exception e) {
      logger.error("Can't register");
      return null;
    }

  }
}
