package com.example.training.service;

import com.example.training.entity.OrderDetail;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The OrderDetailService is the interface write database query methods will be done
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-11-03
 *
 */
public interface OrderDetailService {

  public OrderDetail create(OrderDetail entity);

  public void deleteById(Integer id);

  public OrderDetail update(OrderDetail entity);

  List<OrderDetail> getOrderDetailByOrder(Integer orderid);

  int totalByOrder(Integer productid);

  int sumprice();

  int sumPriceByMonth(int month, int year);
}
