package com.example.training.service.impl;

import com.example.training.entity.Category;
import com.example.training.repositoty.CategoryRepository;
import com.example.training.service.CategoryService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * The CategoryServiceImpl is the class that  implements interface CategoryService
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
@Service
public class CategoryServiceImpl implements CategoryService {

  @Autowired
  private CategoryRepository categoryRepository;

  /**
   * find By Name Category funtion,
   * @param keywords: Search for that name in the database
   */
  @Override
  public Page<Category> findByName(String keywords, Pageable pageable) {
    return categoryRepository.findByName(keywords, pageable);
  }

  /**
   * Get all Category funtion,
   */
  @Override
  public List<Category> findByAll() {
    return categoryRepository.findAll();
  }

  /**
   * Get all Category by Id funtion,
     * @param id: Search for that id in the database
   */
  @Override
  public Optional<Category> findById(Integer id) {
    return categoryRepository.findById(id);
  }
}
