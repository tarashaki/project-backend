package com.example.training.service.impl;

import com.example.training.entity.Product;
import com.example.training.presentation.ProductDTO;
import com.example.training.repositoty.ProductRepository;
import com.example.training.service.ProductService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * The ProductServiceImpl is the class that  implements interface ProductService
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
@Service
public class ProductServiceImpl implements ProductService {

  @Autowired
  private ProductRepository productRepository;

  /**
   * get All products are active funtion,
   * @param page:pagination
   */
  @Override
  public Page<Product> findAllByActive(Pageable page) {
    return productRepository.findAllByActive(page);
  }

  /**
   * get Products by category funtion,
   * @param page:pagination
   * @param id:categoryID
   */
  @Override
  public Page<Product> findByCategoryId(Integer id, Pageable page) {
    return productRepository.findProductByActiveTrueAndCategoryId(id, page);
  }

  /**
   * get products by name funtion,
   * @param pageable:pagination
   * @param keywords: name will be search
   */
  @Override
  public Page<Product> findByNamePage(String keywords, Pageable pageable) {
    return productRepository.findByNamePage(keywords, pageable);
  }

  /**
   * get products by price funtion,
   * @param pageable:pagination
   * @param price: the product below the price
   */
  @Override
  public Page<Product> findByPrice(Integer price, Pageable pageable) {
    return productRepository.findProductByPrice(price, pageable);
  }

  /**
   * create product funtion,
   * @param product: product will be create
   */
  @Override
  public Product create(Product product) {
    product.setActive(true);
    return productRepository.save(product);
  }

  /**
   * update by Order funtion,(minus the number of products)
   * @param product: product will be create
   */
  @Override
  public Product updateByOrder(Product product) {
    product.setQuantity(product.getQuantity() - 1);
    return productRepository.save(product);
  }

  /**
   * update product funtion,
   * @param product: product will be update
   */
  @Override
  public Product update(Product product) {
    product.setActive(true);
    return productRepository.save(product);
  }

  /**
   * delete product funtion,(active= false)
   * @param product: product will be delete
   */
  @Override
  public void delete(Product product) {
    product.setActive(false);
    productRepository.save(product);
  }

  /**
   * get product bi id funtion
   * @param id: productId
   */
  @Override
  public Product findById(Integer id) {
    return productRepository.findProductById(id);
  }

  /**
   * get top4 product best seller funtion
   */
  @Override
  public List<ProductDTO> findTop4Product() {
    List<Object[]> lst = productRepository.findTop4ByActiveTrue(PageRequest.of(0, 4));
    List<ProductDTO> lstDto = new ArrayList<>();
    for (Object[] obj : lst) {
      ProductDTO productDTO = new ProductDTO();
      productDTO.setName((String) obj[0]);
      productDTO.setId((Integer) obj[1]);
      productDTO.setPrice((Integer) obj[2]);
      productDTO.setImage((String) obj[3]);
      lstDto.add(productDTO);
    }
    return lstDto;
  }

  /**
   * search product by name funtion
   * @param keyword: name will be search in menu
   */
  @Override
  public List<Product> findByName(String keyword) {
    return productRepository.findByName(keyword);
  }

  /**
   * get top4 new product funtion
   */
  @Override
  public List<Product> findTop4ByNewProduct() {
    return productRepository.findTop4ByNewProduct(PageRequest.of(0, 4));
  }
}
