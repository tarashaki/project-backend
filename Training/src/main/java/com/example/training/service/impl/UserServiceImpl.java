package com.example.training.service.impl;

import com.example.training.entity.User;
import com.example.training.repositoty.UserRepository;
import com.example.training.service.UserService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * The UserServiceImpl is the class that  implements interface UserService
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-20
 *
 */
@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository userRepository;

  /**
   * get User by Email funtion
   * @param email:email will be search
   */
  @Override
  public Optional<User> findByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  /**
   * check email exists funtion
   * @param email:email will be check
   */
  @Override
  public Boolean existsByEmail(String email) {
    return userRepository.existsByEmail(email);
  }

  /**
   * create user funtion
   * @param user:user will be create
   */
  @Override
  public User create(User user) {
    return userRepository.save(user);
  }

  /**
   * update user funtion
   * @param user:user will be update
   */
  @Override
  public User update(User user) {
    return userRepository.save(user);
  }

  /**
   * delete user funtion
   * @param id:delete user by userid
   */
  @Override
  public void delete(Integer id) {
    userRepository.deleteById(id);
  }

  /**
   * get All user funtion
   * @param page:pagination
   */
  @Override
  public Page<User> findAll(Pageable page) {
    return userRepository.findAll(page);
  }

}
