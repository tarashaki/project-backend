package com.example.training.service.impl;

import com.example.training.entity.Order;
import com.example.training.entity.OrderDetail;
import com.example.training.repositoty.OrderDetailRepository;
import com.example.training.repositoty.OrderRepository;
import com.example.training.service.OrderService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * The OrderServiceImpl is the class that  implements interface OrderService
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-11-03
 *
 */
@Service
public class OrderServiceImpl implements OrderService {

  @Autowired
  private OrderRepository orderRepository;

  @Autowired
  private OrderDetailRepository orderDetailRepository;

  @Override
  public Page<Order> findAll(Pageable pageable) {
    return orderRepository.findByStatusFalse(pageable);
  }

  /**
   * create order and orderDetail funtion,
   * @param orderData:data needed when adding
   */
  @Override
  public Order create(JsonNode orderData) {
    ObjectMapper mapper = new ObjectMapper();
    Order order = mapper.convertValue(orderData, Order.class);
    order.setStatus(false);
    orderRepository.save(order);

    TypeReference<List<OrderDetail>> type = new TypeReference<List<OrderDetail>>() {};
    List<OrderDetail> orderDetails = mapper.convertValue(orderData.get("orderDetails"), type)
            .stream().peek(d -> d.setOrder(order)).collect(Collectors.toList());
    orderDetailRepository.saveAll(orderDetails);
//        orderData.get(orderDetails)
    return order;
  }

  /**
   * order browsing funtion,
   * @param order: order will be approved
   */
  @Override
  public Order updateStatus(Order order) {
    return orderRepository.save(order);
  }

  /**
   * get List Order by User funtion,
   * @param userid: get order by userid
   */
  @Override
  public List<Order> getOrderByUser(Integer userid) {
    return orderRepository.findAllByUserId(userid);
  }

  /**
   * Statistics of all orders funtion,
   */
  @Override
  public int totalOrder() {
    return orderRepository.totalOrder();
  }

}
