package com.example.training.service.impl;

import com.example.training.entity.User;
import com.example.training.repositoty.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * The UserDetailsServiceImpl is the class that  implements interface UserDetailsService
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-22
 *
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired
  UserRepository userRepository;

  /**
   * load User By Username funtion,
   * @param email: find email in database
   */
  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    User user = userRepository.findByEmail(email)
            .orElseThrow(() ->
                    new UsernameNotFoundException("User Not Found with Email : " + email));
    return UserDetailsImpl.build(user);
  }
}
