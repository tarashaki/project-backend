package com.example.training.service;

import com.example.training.common.JwtUtils;
import com.example.training.presentation.LoginRequest;
import com.example.training.repositoty.JwtResponse;
import com.example.training.service.impl.UserDetailsImpl;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * LoginService is a class that helps to perform login function
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-12-15
 *
 */
@Service
public class LoginService {

  @Autowired
  private JwtUtils jwtUtils;

  @Autowired
  private AuthenticationManager authenticationManager;

  /**
   * Login funtion,
   * @param loginRequest: value get when login
   */
  public ResponseEntity<JwtResponse> authenticateUser(@Validated @RequestBody
                                                      LoginRequest loginRequest) {
    Authentication authentication = authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(loginRequest.getEmail(),
                    loginRequest.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateTokenLogin(authentication);

    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
    List<String> roles = userDetails.getAuthorities().stream()
            .map(item -> item.getAuthority())
            .collect(Collectors.toList());

    return ResponseEntity.ok(new JwtResponse(jwt,
            userDetails.getId(), userDetails.getUsername(),
            userDetails.getFirstName(), userDetails.getLastName(),
            userDetails.getBirthday(), roles));
    }
}
