package com.example.training.presentation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The LoginRequest Is the class created to take the value of users in the login
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-23
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequest {

  private String email;

  private String password;
}
