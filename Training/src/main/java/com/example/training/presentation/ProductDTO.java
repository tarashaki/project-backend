package com.example.training.presentation;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
/**
 * The Product Is the class created to take the value of product in the database
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-12-12
 *
 */

@Data
public class ProductDTO {

  private Integer id;

  private String name;

  private int price;

  private String image;

}
