package com.example.training.presentation;

import java.util.Date;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The SignupRequest Is the class created to take the value of users in the register
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-11-18
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignupRequest {

  private String email;

  private String password;

  private String firstName;

  private String lastName;

  private Date birthday;

  private Set<String> role;

}
