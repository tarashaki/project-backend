package com.example.training.presentation;

/**
 * The MessageResponse is the class to create to catch the program error
 *
 * @author nguyenlv
 * @version 1.0
 * @since 2022-10-22
 *
 */
public class MessageResponse {
  private String message;

  public MessageResponse(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
