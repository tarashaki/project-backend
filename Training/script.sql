create table "t_table_category"
(
    "id"   NUMBER(10) generated as identity
        primary key,
    "name" VARCHAR2(255 char)
)
/

create table "t_table_product"
(
    "id"          NUMBER(10) generated as identity
        primary key,
    "active"      NUMBER(1)          not null,
    "image"       VARCHAR2(255 char) not null,
    "name"        VARCHAR2(255 char) not null,
    "price"       NUMBER(10)         not null,
    "category_id" NUMBER(10)         not null
        constraint "FK986a6u2v8nxyk9e50moufg8qg"
            references "t_table_category",
    "quantity"    NUMBER(10)         not null
)
/

create table "t_table_role"
(
    "id"   NUMBER(10) generated as identity
        primary key,
    "name" VARCHAR2(20 char)
)
/

create table "t_table_user"
(
    "id"         NUMBER(10) generated as identity
        primary key,
    "birthday"   DATE,
    "email"      VARCHAR2(255 char),
    "first_name" VARCHAR2(255 char),
    "last_name"  VARCHAR2(255 char),
    "password"   VARCHAR2(255 char)
)
/

create table "t_table_user_role"
(
    "user_id" NUMBER(10) not null
        constraint "FKkp1critevwjnb4welud3j1de3"
            references "t_table_user",
    "role_id" NUMBER(10) not null
        constraint "FKe9xnrkw4j1f6pev0y0xt7dplg"
            references "t_table_role",
    primary key ("user_id", "role_id")
)
/

create table "t_table_order"
(
    "id"           NUMBER(10) generated as identity
        primary key,
    "create_date"  DATE               not null,
    "address"      VARCHAR2(255 char) not null,
    "phone_number" VARCHAR2(255 char) not null,
    "status"       NUMBER(1)          not null,
    "user_id"      NUMBER(10)         not null
        constraint "FK660721wqes3f7lmwwktkar9a4"
            references "t_table_user"
)
/

create table "t_table_orderdetail"
(
    "id"         NUMBER(10) generated as identity
        primary key,
    "image"      VARCHAR2(255 char),
    "price"      NUMBER(10) not null,
    "quantity"   NUMBER(10) not null,
    "order_id"   NUMBER(10)
        constraint "FKqd2fignpldd2o5ngxydheffxv"
            references "t_table_order",
    "product_id" NUMBER(10)
        constraint "FKjpov03c03i2iaalbekgog64gy"
            references "t_table_product"
)
/


